"use strict"

const fs = require("fs")

module.exports.read = file => new Promise((res, rej) => {
  fs.readFile(file, (err, result) => {
    if (err)
      return rej(err)
    
    let data = result.toString()

    if (!file.endsWith(".json"))
      return res(data)

    try {
      let json = JSON.parse(data)
      res(json)
    }

    catch (err) {
      rej(err)
    }
  })
})

module.exports.stat = path => new Promise((res, rej) => {
  fs.stat(path, (err, stat) => {
    err ? rej(err) : res(stat)
  })
})